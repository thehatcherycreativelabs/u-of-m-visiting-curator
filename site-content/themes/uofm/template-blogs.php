<?php
/*
Template Name: Blogs
*/



/*
Content
*/

//$context = Timber::get_context();
//$post = new TimberPost();
//$context['post'] = $post;






/*
Render Template
*/

//if ( post_password_required( $post->ID ) ) {
//	Timber::render( array( 'page-password.twig' ), $context );
//} else {
//	Timber::render(array('template-blogs.twig'), $context);

	$data = Timber::get_context();
	$data['post'] = new TimberPost();
	
	$templates = array('template-blogs.twig');
	if (! is_front_page()){
	
		// get latest three posts
		$args = array(
			'posts_per_page' => 30
		);
		$data['posts'] = Timber::get_posts($args);
	
			// add your twig view to the front of the templates array
		   array_unshift($templates, 'template-blogs.twig.twig');
	}
	
	Timber::render( $templates, $data );