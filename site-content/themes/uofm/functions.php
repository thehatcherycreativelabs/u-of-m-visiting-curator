<?php

/*
WP SETUP
****/
require_once(get_template_directory() . '/includes/remove-wp-junk.php');
require_once(get_template_directory() . '/includes/disable-wp-embed-js.php');
require_once(get_template_directory() . '/includes/disable-wp-emojis.php');
require_once(get_template_directory() . '/includes/admin.php');



/*
TIMBER SETUP
****/
require_once(get_template_directory() . '/includes/timber.php');



/*
ACF SETUP
****/
require_once(get_template_directory() . '/includes/acf.php');


/*
GUTENBERG SETUP
****/
require_once(get_template_directory() . '/includes/gutenberg.php');



/*
WOOCOMMERCE
****/
// require_once(get_template_directory() . '/includes/woo.php');



/*
POST TYPES
****/
require_once('includes/custom-post-type.php');
//require_once('includes/post-post-type.php');





/*
OTHER PLUGIN EXTENDS
****/



/*
MENUS
****/
require_once(get_template_directory() . '/includes/menus.php');



/*
STYLES & SCRIPTS
****/
require_once(get_template_directory() . '/includes/styles-scripts.php');



/*
ADDITIONAL EDITOR FORMATS
****/
require_once(get_template_directory() . '/includes/tinymce.php');


/**
 * Composer (vendor)'
 * - Timber ACF WP Blocks: https://palmiak.github.io/timber-acf-wp-blocks/#/example
 */
require_once 'vendor/autoload.php';