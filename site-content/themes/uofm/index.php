<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package 	WordPress
 * @subpackage 	Timber
 * @since 		Timber 0.1
 */
if (!class_exists('Timber')){
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}


global $paged;
if (!isset($paged) || !$paged){
	$paged = 1;
}

$context = Timber::get_context();
// $context['posts'] = new Timber\PostQuery();

$context['posts'] = new Timber\PostQuery(array(
	// 'category_name'=>'blog',
	'post_status'=>'publish',
	'post_type'=>'post',
	// 'posts_per_page'=>2,
	'paged' => $paged,
	// 'nopaging' => true,
));
$context['categories'] = Timber::get_terms('category');


// create next page url for ajax pagination
// $base_url = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
// $params = preg_replace('(/page/[0-9])', '', $_SERVER['REQUEST_URI']);
// $params = explode('?',$params);
// if( count($params) > 1 ) {
// 	$next_page_url = $base_url.$params[0].'page/'.($paged+1).'/?'.$params[1];
// } else {
// 	$next_page_url = $base_url.$params[0].'page/'.($paged+1).'/';
// }
// $context['next_page_url'] = $next_page_url;



$templates = array('index.twig');
if (is_home()){
	array_unshift($templates, 'home.twig');
}
Timber::render($templates, $context);