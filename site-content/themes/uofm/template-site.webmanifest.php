<?php
/*
Template Name: Webmanifest
*/



/*
Content
*/

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;






/*
Render Template
*/

if ( post_password_required( $post->ID ) ) {
	Timber::render( array( 'page-password.twig' ), $context );
} else {
	Timber::render(array('template-site.webmanifest.twig'), $context);
}