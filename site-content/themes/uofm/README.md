WP Tachyons Theme
===================

https://roperzh.github.io/tachyons-cheatsheet/


Quickstart
----------

Edit broswersync proxy in gulpfile.js to use your local url (eg. https://something.test).

```
npm install -d
gulp
```


Local FlyWheel
--------------

Recommended domain: https://something.test. Probably shouldn't use .dev because it's now a TLD and .local becuase MacOS already uses it.


Gulp
----

### Changes/Optimizations

- one task for css (remoaved concat)
    - style.scss (was named tachyons) is the master file that loads all the partials
- bs injects style.css on change instead of reloading
- bs refreshes on twig now (also php, js)
- removed php, twig tasks (not needed)
- removed pixrem - only needed for ie9, ie10 (if using in font shorthand)


CSS
---

- order of classes should be written using Inverted Triangle CSS
    - Tachyons classes should be defined after the bulk of the themes classes to give it more weight
    - ...except for z-indexes


Tachyons
--------

- https://tachyons.io/
- https://roperzh.github.io/tachyons-cheatsheet/

- `Wordpress Theme Header` is defined in src/custom/_wordpress_theme_header.scss
- `_variables.scss` contains a map of variables used to generate the tachyons classes. Adjust these as required._



To-do
--------

- reduce amount of media queries in tachyons generator. See https://github.com/geoffyuen/hotplate-edge/tree/tachyons_explorer/src/_sass
- switch media queries from ems to px