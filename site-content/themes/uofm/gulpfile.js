var gulp = require("gulp"),
  gutil = require("gulp-util"),
  plumber = require("gulp-plumber"),
  notify = require("gulp-notify"),
  rename = require("gulp-rename"),
  jshint = require("gulp-jshint"),
  concat = require("gulp-concat"),
  uglify = require("gulp-uglify"),
  sass = require("gulp-sass"),
  autoprefixer = require("gulp-autoprefixer"),
  minifyCSS = require("gulp-minify-css"),
  svgstore = require("gulp-svgstore"),
  svgmin = require("gulp-svgmin"),
  cleanCSS = require("gulp-clean-css");
browserSync = require("browser-sync").create();

var onError = function (err) {
  gutil.beep();
  console.log(err);
};

gulp.task("browser-sync", function () {
  var files = ["**/*.php", "views/**/*.twig", "js/**/*.js"];
  browserSync.init(files, {
    proxy: "https://u-of-m.test"
  });
});

gulp.task("scripts", function () {
  gulp
    .src("js/src/*.js")
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(concat("main.js"))
    .pipe(rename({
      suffix: ".min"
    }))
    .pipe(uglify())
    .pipe(gulp.dest("js/"))
    .pipe(notify({
      message: "Scripts Task Complete"
    }));
});

gulp.task("lint", function () {
  gulp
    .src("js/src/main.js")
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(jshint())
    .pipe(jshint.reporter("default"))
    .pipe(notify({
      message: "Lint Task Complete"
    }));
});

gulp.task("styles", function () {
  return gulp
    .src("css/src/style.scss")
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions", "ie 11"],
        cascade: false
      })
    )
    .pipe(cleanCSS({
      compatibility: "ie10"
    }))
    .pipe(minifyCSS())
    .pipe(gulp.dest("./"))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: "Styles Task Complete"
    }));
});

gulp.task("editor-styles", function () {
  return gulp
    .src("css/src/editor.scss")
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        browsers: ["last 2 versions", "ie 11"],
        cascade: false
      })
    )
    .pipe(cleanCSS({
      compatibility: "ie11"
    }))
    .pipe(minifyCSS())
    .pipe(gulp.dest("./"))
    .pipe(browserSync.stream())
    .pipe(notify({
      message: "Editor Styles Task Complete"
    }));
});

// Create svg spritesheet
gulp.task("svgstore", function () {
  return gulp
    .src("img/svg-src/*.svg")
    .pipe(
      plumber({
        errorHandler: notify.onError("svgstore error: <%= error.message %>")
      })
    )
    .pipe(
      svgmin({
        plugins: [{
            removeDoctype: true
          },
          {
            removeComments: true
          },
          {
            removeViewBox: false
          }
        ]
      })
    )
    .pipe(gulp.dest("./img/svg-sprite/"))
    .pipe(svgstore())
    .pipe(gulp.dest("./img/svg"))
    .pipe(notify("SVG'ed"));
});

gulp.task(
  "default",
  ["lint", "scripts", "styles", "editor-styles", "svgstore", "browser-sync"],
  function () {
    gulp.watch("css/src/**/*", ["styles", "editor-styles"]);
    gulp.watch("js/src/*", ["lint", "scripts"]);
    gulp.watch("img/svg-src/*.svg", ["svgstore"]);
  }
);