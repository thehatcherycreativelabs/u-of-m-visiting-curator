<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /functions sub-directory
 *
 * @package 	WordPress
 * @subpackage 	Timber
 * @since 		Timber 0.2
 */
global $paged;
if (!isset($paged) || !$paged){
	$paged = 1;
}


$templates = array('archive.twig', 'index.twig');
$context = Timber::get_context();
$context['title'] = 'Archive';
if (is_category()){
	$context['title'] = single_cat_title('', false);
	array_unshift($templates, 'archive-'.get_query_var('cat').'.twig');
}
$context['posts'] = new Timber\PostQuery(array(
	'cat'=>get_query_var('cat'),
	'post_status'=>'publish',
	'post_type'=>'post',
	// 'posts_per_page'=>2,
	'paged' => $paged,
	// 'nopaging' => true,
));

$context['categories'] = Timber::get_terms('category');
$context['term_page'] = new TimberTerm();

Timber::render($templates, $context);
