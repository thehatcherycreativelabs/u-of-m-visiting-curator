<?php
// function ver($relative_path) {
// 	return filemtime(get_template_directory() . '/' . $relative_path);
// }
// Make the editor wider with css
add_action('enqueue_block_editor_assets', function () {
	// wp_enqueue_script(
	// 	'theme-editor',
	// 	get_template_directory_uri() . '/js/gutenburg.js',
	// 	array('wp-blocks', 'wp-dom'),
	// 	ver('/js/gutenburg.js'),
	// 	true
	// );

	// add the theme.path obj to let gutenberg load the svg spritesheet
	wp_enqueue_script(
		'theme-path',
		get_template_directory_uri() . '/themepath.js.php',
		[],
		'1.0',
		true
	);

});


// Limit gutenberg blocks
function hatch_allowed_block_types($allowed_blocks, $post)
{
	$allowed_blocks = array(
		'core/image',
		'core/paragraph',
		'core/heading',
		'core/html',
		'core/list',
		'core/gallery',
		'core/quote',
		'core/separator',
		'core/table',
		'core/media-text',
		'core/columns',
		'core/spacer',
		'core/embed', // shows *all* embeds -
		// These don't work:
		// 'core-embed/youtube',
		// 'core/embed/youtube',
		// 'core/embed-youtube',
		// 'core-embed/facebook',
		// 'core-embed/twitter',
		// 'core-embed/instagram',
		// 'core/embed/facebook',
		// 'core/embed/twitter',
		// 'core/embed/instagram',
		'gravityforms/form'
	);
	// Get all our ACF Blocks and whitelist them
	// https://github.com/palmiak/timber-acf-wp-blocks
	$blocks = glob( get_template_directory() . "/views/blocks/*.twig" );
	foreach ($blocks as $block) {
		$file = pathinfo($block);
		$allowed_blocks[] = "acf/" . $file['filename'];
	}

	// if( $post->post_type === 'page' ) {
	//   $allowed_blocks[] = 'acf/hero';
	// }

	return $allowed_blocks;
}
// add_filter( 'allowed_block_types', 'hatch_allowed_block_types', 10, 2 );


/**
 * FILTERS
 * - preprocess data with php and make avaiable to all or some blocks
 * - https://palmiak.github.io/timber-acf-wp-blocks/#/filters
 */

// Let blocks access the post's data (eg post.title, post.thumbnail, ACF fields, etc)
// - Will only work when post is saved so do error handling!
add_filter( 'timber/acf-gutenberg-blocks-data', function( $context ){
	if ( is_admin() && function_exists( 'acf_maybe_get_POST' ) ) {
		// Get post id while in editor
		$id = intval( acf_maybe_get_POST( 'post_id' ) );
		$context['post'] = Timber::get_post($id);
	} else {
		// Just get the post is on the frontend
		$context['post'] = Timber::get_post();
	}
	return $context;
} );