<?php
/*
ADD WOOCOMMERCE THEME SUPPORT
****/
function hatch_add_woocommerce_support()
{
	add_theme_support('woocommerce');
}
add_action('after_setup_theme', 'hatch_add_woocommerce_support');


/*
FIX WOOCOMMERCE ARCHIVE LOOP when using Timber
****/
// Call this inside your loop, before rendering product content:
// `{{ fn('timber_set_product', post) }}`
// https://github.com/timber/timber/blob/master/docs/guides/woocommerce.md
function timber_set_product( $post ) {
    global $product;
    if ( is_woocommerce() ) {
        $product = wc_get_product( $post->ID );
    }
}


/*
REMOVE SYTLES
****/
add_filter( 'woocommerce_enqueue_styles', '__return_false' );

/**
* DISABLE UNUSED SCRIPTS
*/
function hatch_woo_rm_scripts()
{
    wp_dequeue_script('fancybox');
    wp_dequeue_script('prettyPhoto');
    wp_dequeue_script('prettyPhoto-init');
}
add_action('wp_enqueue_scripts', 'hatch_woo_rm_scripts');


/*
SUPPORT PRODUCT_VISIBILITY => HIDDEN : hide from product post loops
****/
function hatch_hide_hidden_product( $query ){
	// Hide from search
    if ( !is_admin() && $query->is_main_query() && (is_post_type_archive('product') || is_search()) ) {
        $tax_query = $query->get('tax_query', array());
        $tax_query[] = array(
            'taxonomy' => 'product_visibility',
            'field'    => 'name',
            'terms'    => 'exclude-from-catalog',
            'operator' => 'NOT IN',
        );
        $query->set('tax_query', $tax_query);
	}
	// Hide from shop
	if ( !is_admin() && $query->is_main_query() && is_post_type_archive('product') ) {
        $query->set('post_status', 'publish');
        $query->set('orderby', 'menu_order');
        $query->set('order', 'ASC');
        $query->set('posts_per_page', -1);
	}
}
add_action('pre_get_posts','hatch_hide_hidden_product');


/*
AJAX UPDATE CART in NAV
****/
add_filter( 'woocommerce_add_to_cart_fragments', 'hatch_wc_update_cart_count' );
add_action( 'wp_ajax_product_remove', 'hatch_wc_update_cart_count' );
add_action( 'wp_ajax_nopriv_product_remove', 'hatch_wc_update_cart_count' );
function hatch_wc_update_cart_count( $fragments ) {
	$fragments['span#cart_count'] = '<span id="cart_count">' . WC()->cart->get_cart_contents_count() . '</span>';
	return $fragments;
}