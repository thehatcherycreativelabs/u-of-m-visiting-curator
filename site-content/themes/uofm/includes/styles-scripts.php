<?php

// ENQUEUE STYLESHEET
function my_enqueue_style() {
    wp_enqueue_style( 'my-styles', get_stylesheet_uri(),'', filemtime( get_stylesheet_directory() . '/style.css') );
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_style' );



function my_theme_scripts() {

	wp_enqueue_script('jquery');

	// other scripts
	wp_enqueue_script('scripts', get_template_directory_uri() . '/js/main.min.js' , '' , filemtime( get_template_directory() . '/js/main.min.js'), true);

	// ajax with other scripts
	wp_localize_script( 'scripts', 'my_ajax_object', array(
			'my_ajax_url' => admin_url( 'admin-ajax.php' ),
		)
	);
}
add_action( 'wp_enqueue_scripts', 'my_theme_scripts' );



// ADD EDITOR STYLES
function my_add_editor_styles() {
    add_editor_style( 'editor.css' );
}
add_action( 'admin_init', 'my_add_editor_styles' );