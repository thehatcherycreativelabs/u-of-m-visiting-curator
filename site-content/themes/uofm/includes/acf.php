<?php
// Create ACF Options Page(s)
if( function_exists('acf_add_options_sub_page') )
{
	acf_add_options_page();
	foreach (
		[
			'General', 
			'Site', 
			'Contact'

		]
	as $page_title) {
		acf_add_options_sub_page(['page_title' => $page_title]);
	}
}


// Gmap api key for ACF fields editors
//function hatch_acf_init() {
//	acf_update_setting('google_api_key', 'AIzaSyC3coGeNU1TrE-EFaWypZ8UcpXH0fezINI');
//}
//add_action('acf/init', 'hatch_acf_init');



// Create Gutenberg blocks
//function hatch_register_blocks() {

	// check function exists
	//if( function_exists('acf_register_block') ) {

		// hero block
		//acf_register_block(array(
		//	'name'				=> 'hero',
		//	'title'				=> __('Hero'),
		//	'description'		=> __('A custom hero block.'),
		//	'render_callback'	=> 'hatch_render_acf_hero_block',
		//	'category'			=> 'layout-elements',
		//	'icon'				=> 'welcome-view-site',
		//	'keywords'			=> array( 'hero', 'banner' ),
		//	'post_types' 		=> array( 'page')
		//));

	//}
//}
//add_action('acf/init', 'hatch_register_blocks');


// Hero block render
//function hatch_render_acf_hero_block( $block, $content = '', $is_preview = false ) {

//	$context = Timber::context();
//	$context['css'] = dfm_tachyons_classes();
//	$context['block'] = $block;
//	$context['fields'] = get_fields();
//	$context['is_preview'] = $is_preview;

//	Timber::render( 'blocks/hero.twig', $context );
//}