<?php


// Add styles dropdown to TinyMCE for custom styles
function my_mce_editor_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}
add_filter( 'mce_buttons_2', 'my_mce_editor_buttons' );


function my_mce_before_init( $settings ) {

	$style_formats = array(
		array(
			'title' => 'Button - Navy',
			'selector' => 'a',
			'classes' => 'btn'
		),
	);

	$settings['style_formats'] = json_encode( $style_formats );

	return $settings;
}
add_filter( 'tiny_mce_before_init', 'my_mce_before_init' );



function my_remove_tinymce_row2_btns( $buttons ) {
    $remove_buttons = array(
        'forecolor', // text color
    );
    foreach ( $buttons as $button_key => $button_value ) {
        if ( in_array( $button_value, $remove_buttons ) ) {
            unset( $buttons[ $button_key ] );
        }
    }
    return $buttons;
}
add_filter( 'mce_buttons_2', 'my_remove_tinymce_row2_btns');