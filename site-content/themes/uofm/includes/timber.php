<?php
if (!class_exists('Timber')){
	add_action( 'admin_notices', function(){
		echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . admin_url('plugins.php#timber') . '">' . admin_url('plugins.php') . '</a></p></div>';
	});
	return;
}


function hatch_tachyons_classes() {

	$css = array(
		'container' => 'mw6 center ph4',
		'btn' => 'f10 f9-m f8-xxl dib pv3 ph5 tc br4 ttu fw700 tracked h50 h58-xl hover-btn',
	);

	return $css;
};



class StarterSite extends TimberSite {

	function __construct(){
		add_theme_support('post-thumbnails');
		add_theme_support('menus');
		add_filter('timber_context', array($this, 'add_to_context'));
		add_filter('get_twig', array($this, 'add_to_twig'));
		parent::__construct();
	}

	function add_to_context($context){
		$context['menu_primary'] = new TimberMenu('primary');
		$context['menu_secondary'] = new TimberMenu('secondary');
		$context['site'] = $this;
		$context['options'] = get_fields('option');
		$context['css'] = hatch_tachyons_classes();
		return $context;
	}

	function add_to_twig($twig){
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension(new Twig_Extension_StringLoader());
		// $twig->addFilter('myfoo', new Twig_Filter_Function('myfoo'));
		return $twig;
	}

}

new StarterSite();