<?php

function exhibition_post_type() {
  $labels = array(
	'name' => _x('Exhibitions', 'post type general name'),
	'singular_name' => _x('Exhibition', 'post type singular name'),
	'add_new' => _x('Add New', 'Exhibition'),
	'add_new_item' => __('Add New Exhibition'),
	'edit_item' => __('Edit Exhibition'),
	'new_item' => __('New Exhibition'),
	'all_items' => __('All People'),
	'view_item' => __('View Exhibition'),
	'search_items' => __('Search Exhibitions'),
	'not_found' =>  __('No Exhibitions Found'),
	'not_found_in_trash' => __('No Exhibitions Found in Trash'),
	'parent_item_colon' => '',
	'menu_name' => 'Exhibitions'

  );
  $args = array(
	'labels' => $labels,
	'public' => true,
	'publicly_queryable' => true,
	'show_ui' => true,
	'show_in_menu' => true,
	'show_in_rest' => true,
	'query_var' => true,
	'rewrite' => array( 'slug' => 'exhibition' ),
	'capability_type' => 'post',
	'has_archive' => false,
	'hierarchical' => true,
	'menu_position' => null,
	'menu_icon' => 'dashicons-format-gallery',
	'supports' => array( 'title', 'page-attributes', 'editor', 'thumbnail' )
  );
  register_post_type('um_exhibition',$args);
}
add_action( 'init', 'exhibition_post_type' );
