<?php

// REFRESH ON SAVE
function hatch_browsersync_helper() {
	if (WP_DEBUG) {
		touch( get_template_directory() . '/index.html');
	}
}
add_action( 'save_post', 'hatch_browsersync_helper' );


// STICK ADMIN UPDATE BUTTON TO TOP RIGHT
add_action('admin_head', 'hatch_add_admin_css');
function hatch_add_admin_css() {
	echo '<style>
#publish {
	position: fixed;
	top: 3.875rem;
	right: 1.25rem;
	z-index: 16000;
}
</style>';
}